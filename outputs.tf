output "bucket_domain_name" {
  value       = "${var.enabled == "true" ? join("", aws_s3_bucket.this.*.bucket_domain_name) : ""}"
  description = "FQDN of bucket"
}

output "bucket_id" {
  value       = "${var.enabled == "true" ? join("", aws_s3_bucket.this.*.id) : ""}"
  description = "Bucket Name (aka ID)"
}

output "bucket_arn" {
  value       = "${var.enabled == "true" ? join("", aws_s3_bucket.this.*.arn) : ""}"
  description = "Bucket ARN"
}

output "enabled" {
  value       = "${var.enabled}"
  description = "Is module enabled"
}